set(anchorman_SRCS
	anchorman.c
)

include_directories(${GSTREAMER_INCLUDE_DIRS} ${GUDEV_INCLUDE_DIRS})

add_executable(anchorman ${anchorman_SRCS})

target_link_libraries(anchorman ${GSTREAMER_LIBRARIES} ${GUDEV_LIBRARIES})

install(TARGETS anchorman DESTINATION bin)
